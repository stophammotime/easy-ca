package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/spacemonkeygo/openssl"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	getCommand := flag.NewFlagSet("get", flag.ExitOnError)
	createCommand := flag.NewFlagSet("create", flag.ExitOnError)

	// Create Commands
	root := createCommand.Bool("root", false, "Create's a Root Certificate Authority.")

	switch os.Args[1] {
	case "get":
		getCommand.Parse(os.Args[2:])
	case "create":
		createCommand.Parse(os.Args[2:])
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}

	if getCommand.Parsed()  {
		fmt.Println("Looks like we're going to 'get'!")
	}

	if createCommand.Parsed()  {
		fmt.Println("Looks like we're going to 'create'!")

		if *root {
			CreateRootCA()
		}
	}
}

func CreateRootCA() {
	root_name := GetInput("What is the name of your CA?")
	root_name = strings.TrimSuffix(root_name, "\n")
	os.Mkdir(root_name, 0744)
	fmt.Print("We're going to create a root CA.\n")

	key, _ := openssl.GenerateRSAKey(4096)
	pem, _ := key.MarshalPKCS1PrivateKeyPEM()

	fmt.Printf("Output Directory: %s\n", root_name + "/root.private.key.pem")

	err := ioutil.WriteFile(root_name + "/root.private.key.pem", pem, 0744)
	CheckError(err)
}

func GetInput(message string) (string) {
	if len(message) > 0 {
		fmt.Print(message + " ")
	}

	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')

	return text
}

func CheckError(e error) {
	if e != nil {
		panic(e)
	}
}